# README #

### Taco Software Engineer Practical Test ###

* Implement a software to calculate the print costs in C# language.
* .NET Framework version: 4.6.1
* Visual Studio version: 2017
* This solution includes 3 projects which are PaperCut, UnitTest and Taco
* PaperCut project implements the required business logic.
* Taco project implements the console application to out put the required data.
* UnitTest project implements unit testing methods.

### How to run the console application ###

* The console application is implemented in Taco project. In Visual Studio, set the project as StartUp Project then Start debugging (F5).
* In Main method, the relative paths in priceSheetPath and jobsPath variable are pointing to sample files in the "Sample" folder located in the same directory as the Taco.sln file. Change these paths for your testing if necessary.
