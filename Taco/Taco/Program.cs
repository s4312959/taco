﻿using PaperCut;
using System;
using System.Linq;

namespace Taco
{
    class Program
    {
        static void Main(string[] args)
        {
            const string priceSheetPath = @"..\..\..\Sample\PriceSheet.csv";
            const string jobsPath = @"..\..\..\Sample\sample1.csv";

            var printer = new Printer(priceSheetPath);
            printer.LoadJobs(jobsPath);
            double sum = 0;
            Console.WriteLine("Total Page" + "\t" + "Color Page" + "\t" + "Job Type" + "\t" + "Cost");
            foreach (var job in printer.Jobs.Where(j => j.IsValid))
            {
                double cost = printer.GetJobPrice(job) / 100;
                sum += cost;
                Console.WriteLine(job.TotalPage + "\t\t" + job.ColorPage + "\t\t" + job.JobType.ToString() + "\t" + cost.ToString("C"));
            }

            Console.WriteLine($"Total cost: {sum.ToString("C")}");
            Console.WriteLine($"Total jobs: {printer.Jobs.Count}. Total valid jobs: {printer.Jobs.Count(j => j.IsValid)}");

            Console.ReadLine();
        }
    }
}
