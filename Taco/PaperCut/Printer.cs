﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PaperCut
{
    public enum PageSize { A4 }

    public enum JobType { SingleSided, DoubleSided }

    public enum PageType { BlackWhite, Colour }

    public class Printer
    {
        public List<Price> Prices = new List<Price>();

        public List<PrintJob> Jobs = new List<PrintJob>();

        public Printer(string printSheetPath)
        {
            Prices = File.ReadAllLines(printSheetPath)
                         .Skip(1)
                         .Select(p => Price.FromCsv(p))
                         .Where(p => p.IsValid)
                         .ToList();
        }

        public void LoadJobs(string jobPath)
        {
            Jobs = File.ReadAllLines(jobPath)
                       .Skip(1)
                       .Select(j => PrintJob.FromCsv(j))
                       .ToList();
        }

        public double GetPricePerPage(PageSize pageSize, JobType jobType, PageType pageType)
        {
            var price = Prices.Where(p => p.PageSize == pageSize
                                       && p.JobType == jobType
                                       && p.PageType == pageType).FirstOrDefault();

            if (price != null) return price.Amount;

            throw new ArgumentOutOfRangeException("No price found.");
        }

        public double GetJobPrice(PrintJob job)
        {
            if (!job.IsValid)
                throw new Exception("Job is invalid");

            double sum = 0;
            int bwp = job.TotalPage - job.ColorPage;
            sum += bwp > 0 ? bwp * GetPricePerPage(job.PageSize, job.JobType, PageType.BlackWhite) : 0;
            sum += job.ColorPage > 0 ? job.ColorPage * GetPricePerPage(job.PageSize, job.JobType, PageType.Colour) : 0;

            return sum;
        }
    }
}
