﻿namespace PaperCut
{
    public class Price
    {
        public JobType JobType { get; set; }

        public PageSize PageSize { get; set; }

        public PageType PageType { get; set; }

        public double Amount { get; set; }

        public bool IsValid { get; set; } = true;

        public static Price FromCsv(string line)
        {
            string[] values = line.Split(',');
            var price = new Price();

            try
            {
                // page size
                if (values[0] == "A4") price.PageSize = PageSize.A4;

                // color
                price.PageType = values[1].ToLower().Trim() == "true" ? PageType.Colour : PageType.BlackWhite;

                // double size
                price.JobType = values[2].ToLower().Trim() == "true" ? JobType.DoubleSided : JobType.SingleSided;

                // price
                price.Amount = double.Parse(values[3]);
            }
            catch
            {
                price.IsValid = false;
            }

            return price;
        }
    }
}
