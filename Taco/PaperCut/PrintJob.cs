﻿namespace PaperCut
{
    public class PrintJob
    {
        public int TotalPage { get; set; }

        public int ColorPage { get; set; }

        public JobType JobType { get; set; }

        public PageSize PageSize { get; set; }

        public bool IsValid { get; set; } = true;

        public static PrintJob FromCsv(string line)
        {
            string[] values = line.Split(',');
            var job = new PrintJob();

            job.PageSize = PageSize.A4;

            try
            {
                // total pages
                job.TotalPage = int.Parse(values[0]);

                // color pages
                job.ColorPage = int.Parse(values[1]);

                // double size
                job.JobType = values[2].ToLower().Trim() == "true" ? JobType.DoubleSided : JobType.SingleSided;

                if (job.ColorPage > job.TotalPage || job.ColorPage < 0 || job.TotalPage < 0)
                    job.IsValid = false;
            }
            catch
            {
                job.IsValid = false;
            }

            return job;
        }
    }
}
