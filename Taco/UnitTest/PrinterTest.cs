﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PaperCut;

namespace UnitTest
{
    [TestClass]
    public class PrinterTest
    {
        [TestMethod]
        public void TestLoadPriceSheet()
        {
            var printer = new Printer(@"..\..\Sample\PriceSheet.csv");

            Assert.AreEqual(4, printer.Prices.Count);

            Assert.AreEqual(15, printer.GetPricePerPage(PageSize.A4, JobType.SingleSided, PageType.BlackWhite));
            Assert.AreEqual(25, printer.GetPricePerPage(PageSize.A4, JobType.SingleSided, PageType.Colour));
            Assert.AreEqual(10, printer.GetPricePerPage(PageSize.A4, JobType.DoubleSided, PageType.BlackWhite));
            Assert.AreEqual(20, printer.GetPricePerPage(PageSize.A4, JobType.DoubleSided, PageType.Colour));
        }

        [TestMethod]
        public void TestLoadJobs()
        {
            var printer = new Printer(@"..\..\Sample\PriceSheet.csv");
            printer.LoadJobs(@"..\..\Sample\sample1.csv");

            Assert.AreEqual(4, printer.Jobs.Count);

            Assert.AreEqual(25, printer.Jobs[0].TotalPage);
            Assert.AreEqual(10, printer.Jobs[0].ColorPage);
            Assert.AreEqual(JobType.SingleSided, printer.Jobs[0].JobType);

            Assert.AreEqual(55, printer.Jobs[1].TotalPage);
            Assert.AreEqual(13, printer.Jobs[1].ColorPage);
            Assert.AreEqual(JobType.DoubleSided, printer.Jobs[1].JobType);
        }

        [TestMethod]
        public void TestGetJobPrice()
        {
            var printer = new Printer(@"..\..\Sample\PriceSheet.csv");
            printer.LoadJobs(@"..\..\Sample\sample1.csv");

            Assert.AreEqual(475, printer.GetJobPrice(printer.Jobs[0]));
        }

        [TestMethod]
        public void TestTotalJobPrice()
        {
            var printer = new Printer(@"..\..\Sample\PriceSheet.csv");
            printer.LoadJobs(@"..\..\Sample\sample1.csv");

            double sum = 0;
            foreach (var job in printer.Jobs)
            {
                sum += printer.GetJobPrice(job);
            }

            Assert.AreEqual(6410, sum);
        }
    }
}
